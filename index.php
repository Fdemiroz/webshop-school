<!DOCTYPE HTML>
<html>
	<head>
	<?php include ('db.php'); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Webshop </title>
	<!-- 
	//////////////////////////////////////////////////////

		Faruk

	//////////////////////////////////////////////////////
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">
	<!Theme MAIN.css>
	<link rel="stylesheet" href="css/main.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<!--LOADER-->	
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<!NAVBAR!>
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<!LOGO!>
			<div class="fh5co-top-logo">
				<div id="fh5co-logo"><a href="index.php">Webshop</a></div>
			</div>
			<!MENUCENTER>
			<div class="fh5co-top-menu menu-1 text-center">
				<ul>
					<!-- <li><a href="work.html">Work</a></li> about.html-->
					<li class="active"><a href="index.php">Home</a></li>
					<li class="has-dropdown">
						<a class="specialeffect" href="#">Categorie</a>
						<ul class="dropdown">
							<li><a href="#">Laptops</a></li>
							<li><a href="#">Muizen</a></li>
							<li><a href="#">Laptoptassen</a></li>
						</ul>
					</li>
					<li><a href="contact.php" style="">Contact</a></li>
					<li class="has-dropdown">
					<!LOGIN!>
						<a  href="login.php">Login</a>
						<ul class="dropdown">
							<li>
								<div id="login">
								<form action="" method="POST">
									<label class="Gebruikersnaam">Gebruikersnaam</label>
									<input id="name" name="username" placeholder="" type="text">
									<label class="Wachtwoord">Wachtwoord</label>
									<input id="password" name="password" placeholder="" type="password">
									<input name="submit" type="submit" value=" Login ">
									<!--ERROR LOGIN-->
									<span><?php echo $error; ?></span>
								</form>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<!SOCIAL   									>
			<div class="fh5co-top-social menu-1 text-right">
				<ul class="fh5co-social">
						<li><a href="https://twitter.com/?lang=nl"><i class="icon-twitter"></i></a></li>
						<li><a href="https://www.facebook.com/"><i class="icon-facebook"></i></a></li>
						<li><a href="https://nl.linkedin.com/"><i class="icon-linkedin"></i></a>
				</ul>
			</div>
		</div>
	</nav>
	<!ENDNAVBAR!>
	<div id="fh5co-work">
		<div class="container">
			<div class="row top-line animate-box">
				<div class="col-md-7 col-md-push-5 text-left intro">
					<h2>Onze producten zijn <span class="fh5co-highlight">met <i class="icon-heart2"></i> voor u uitgekozen.</span></h2>
					<h3>€ 50,- korting op de onderstaande laptops</h3>
				</div>
			</div>
			<!Presentatie aanbiedingen>
			<div class="row">
			<!FIRST>
				<div class="col-md-4 text-center animate-box">
					<a class="work" href="product_detail.php">
						<div class="work-grid" style="background-image:url(images/lenovo.jpg); background-size: contain;">
							<div class="inner">
								<div class="desc">
								<h3><?php 
										$producten = $mysqli->query("SELECT * FROM db_producten WHERE id= 1;");

										while($row = $producten->fetch_assoc()){
											echo '<br>';
											echo '<br>';
											// echo $row['id'].'<br>';
											echo $row['p_naam'].'<br>';
											echo '<br>';
											echo 'van ';
											// echo $row['p_omschrijvijng'].'<br>';
											echo '€ 749,- ';
											echo '<br>';
											echo 'voor ';
											echo '€ '; echo $row['p_prijs'].'<br>';
											}
								?></h3>
								<!PRODUCTKNOP!>
								<span class="cat"><button class="productknop">Klik hier</span></button> 
							</div>
							</div>
						</div>
					</a>
				</div>
				<!SECCOND>
				<div class="col-md-4 text-center animate-box">
					<a class="work" href="product_detail.php">
						<div class="work-grid" style="background-image:url(images/asus.jpg); background-size: contain;">
							<div class="inner">
								<div class="desc">
									<h3><?php 
										$producten = $mysqli->query("SELECT * FROM db_producten WHERE id= 2;");
										while($row = $producten->fetch_assoc()){
											echo '<br>';
											echo '<br>';
											// echo $row['id'].'<br>';
											echo $row['p_naam'].'<br>';
											echo '<br>';
											echo 'van ';
											// echo $row['p_omschrijvijng'].'<br>';
											echo '€ 599,- ';
											echo '<br>';
											echo 'voor ';
											echo '€ '; echo $row['p_prijs'].'<br>';
											}
								?></h3>
									<span class="cat">Klik hier</span>
								</div>
							</div>
						</div>
					</a>
				</div>
				<!THIRD>
				<div class="col-md-4 text-center animate-box">
					<a class="work" href="product_detail.php">
						<div class="work-grid" style="background-image:url(images/hp.jpg); background-size: contain;">
							<div class="inner">
								<div class="desc">
									<h3><?php 
										$producten = $mysqli->query("SELECT * FROM db_producten WHERE id= 3;");
										while($row = $producten->fetch_assoc()){
											echo '<br>';
											echo '<br>';
											// echo $row['id'].'<br>';
											echo $row['p_naam'].'<br>';
											echo '<br>';
											echo 'van ';
											// echo $row['p_omschrijvijng'].'<br>';
											echo '€ 649,- ';
											echo '<br>';
											echo 'voor ';
											echo '€ '; echo $row['p_prijs'].'<br>';
											}
								?></h3>
									<span class="cat">Logo</span>
								</div>
							</div>
						</div>
					</a>
				</div>
				
	<! MID SECTION>
	<div id="fh5co-author" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>A little about me</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="author">
						<div class="author-inner animate-box" style="background-image: url(images/person3.jpg);">
						</div>
						<div class="desc animate-box">
							<span>Web Designer &amp; Developer</span>
							<h3>Mike Airways</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
							<p><a href="about.html" class="btn btn-primary btn-outline">Learn More</a></p>
							<ul class="fh5co-social-icons">
								<li><a href="https://twitter.com/?lang=nl"><i class="icon-twitter"></i></a></li>
								<li><a href="https://www.facebook.com/"><i class="icon-facebook"></i></a></li>
								<li><a href="https://nl.linkedin.com/"><i class="icon-linkedin"></i></a>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-started">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Bestel je voor het eerst?</h2>
					<!REGISTER!>
					<p><a href="register.php" style="color: black;">Meld je aan als nieuwe klant.</a></p>
					<p><a href="register.php" class="btn btn-primary">Registreer</a></p>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Saxion. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="contact.html" target="_blank">Contact</a></small>
					</p>
					
					<ul class="fh5co-social-icons">
						<p>Subscribe</p>
						<li><a href="https://twitter.com/?lang=nl"><i class="icon-twitter"></i></a></li>
						<li><a href="https://www.facebook.com/"><i class="icon-facebook"></i></a></li>
						<li><a href="https://nl.linkedin.com/"><i class="icon-linkedin"></i></a>
					</ul>				
				</div>
			</div>
		</div>
		<! END FOOTER>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

