<!DOCTYPE HTML>
<html>
	<head>
	<?php include ('db.php'); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Webshop </title>
	<!-- 
	//////////////////////////////////////////////////////

		Faruk

	//////////////////////////////////////////////////////
		
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<!NAVBAR!>
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<!LOGO!>
			<div class="fh5co-top-logo">
				<div id="fh5co-logo"><a href="index.php">Webshop</a></div>
			</div>
			<!MENUCENTER>
			<div class="fh5co-top-menu menu-1 text-center">
				<ul>
					<!-- <li><a href="work.html">Work</a></li> about.html-->
					<li><a href="index.php">Home</a></li>
					<li class="has-dropdown">
						<a href="#">Categorie</a>
						<ul class="dropdown">
							<li><a href="#">Laptops</a></li>
							<li><a href="#">Muizen</a></li>
							<li><a href="#">Laptoptassen</a></li>
						</ul>
					</li>
					<li><a href="contact.html">Contact</a></li>
					<li class="has-dropdown">
					<!LOGIN!>
						<a href="login.php">Login</a>
						<ul class="dropdown">
							<li>
								<div id="login">
								<form action="" method="POST">
									<label class="Gebruikersnaam">Gebruikersnaam</label>
									<input id="name" name="username" placeholder="" type="text">
									<label class="Wachtwoord">Wachtwoord</label>
									<input id="password" name="password" placeholder="" type="password">
									<input name="submit" type="submit" value=" Login ">
									<!--ERROR LOGIN-->
									<span><?php echo $error; ?></span>
								</form>
								</div>
							</li>
						</ul>
						</li>
				</ul>
			</div>
			<!SOCIAL   									>
			<div class="fh5co-top-social menu-1 text-right">
				<ul class="fh5co-social">
					<li><a href="https://twitter.com/?lang=nl"><i class="icon-twitter"></i></a></li>
						<li><a href="https://www.facebook.com/"><i class="icon-facebook"></i></a></li>
						<li><a href="https://nl.linkedin.com/"><i class="icon-linkedin"></i></a>
				</ul>
			</div>
			
		</div>
	</nav>
	<! END NAVBAR>
	<div id="fh5co-contact">
		<div class="container">
			<div class="row top-line animate-box">
				<div class="col-md-6 col-md-offset-3 col-md-push-2 text-left fh5co-heading">
					<h2>Say Hello</h2>
					<h3>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-5 col-md-pull-1 animate-box">
						<div class="fh5co-contact-info">
							<h3>Contact Information</h3>
							<ul>
								<li class="address">198 West 21th Street, <br> Suite 721 New York NY 10016</li>
								<li class="phone"><a href="tel://1234567920">+ 1235 2355 98</a></li>
								<li class="email"><a href="mailto:info@yoursite.com">info@yoursite.com</a></li>
								<li class="url"><a href="http://freehtml5.co">FreeHTML5.co</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-7 animate-box">
						<h3>Get In Touch</h3>
						<form action="#">
							<div class="row form-group">
								<div class="col-md-12">
									<label for="fname">Your Name</label>
									<input type="text" id="fname" class="form-control">
								</div>
								
							</div>

							<div class="row form-group">
								<div class="col-md-12">
									<label for="email">Email</label>
									<input type="text" id="email" class="form-control">
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-12">
									<label for="message">Message</label>
									<textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group">
								<input type="submit" value="Send Message" class="btn btn-primary">
							</div>

						</form>		
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- REGISTREER-->
	<div id="fh5co-started">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Bestel je voor het eerst?</h2>
					<!REGISTER!>
					<p><a href="register.php" style="color: black;">Meld je aan als nieuwe klant.</a></p>
					<p><a href="register.php" class="btn btn-primary">Registreer</a></p>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Saxion. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="contact.html" target="_blank">Contact</a></small>
					</p>
					
					<ul class="fh5co-social-icons">
						<p>Subscribe</p>
						<li><a href="https://twitter.com/?lang=nl"><i class="icon-twitter"></i></a></li>
						<li><a href="https://www.facebook.com/"><i class="icon-facebook"></i></a></li>
						<li><a href="https://nl.linkedin.com/"><i class="icon-linkedin"></i></a>
					</ul>				
				</div>
			</div>
		</div>
		<! END FOOTER>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

